import copy
import http
import os
import webbrowser
from http.cookiejar import CookieJar

import requests
import selenium
from bs4 import BeautifulSoup
import math
from datetime import datetime
from multiprocessing.dummy import Pool as ThreadPool
from Building import Building
from selenium import webdriver
from http import cookiejar

COOKIES_ALL = {}
COOKIES_NV = {}
WD = webdriver.PhantomJS()
BUILDING_CURR = 1
BUILDINGS_TOTAL = 0
TIMEOUT = 20
THREAD_COUNT = 4
HEADERS = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
                            'AppleWebKit/537.36 (KHTML, like Gecko) '
                            'Chrome/63.0.3239.84 Safari/537.36'}

#####################################################################
######################## GENERAL METHODS ############################
#####################################################################

def login(username, password):
    global COOKIES_ALL, COOKIES_NV, WD, CJ

    wd = webdriver.PhantomJS()
    wd.get("https://findbolig_old.nu/logind.aspx")
    wd.find_element_by_id("ctl00_placeholdercontent_1_txt_UserName").send_keys(username)
    wd.find_element_by_id("ctl00_placeholdercontent_1_txt_Password").send_keys(password)
    wd.find_element_by_id("ctl00_placeholdercontent_1_but_LoginShadow").click()

    for cookie in wd.get_cookies():
        if (cookie['name'] == "ASP.NET_SessionId") or (cookie['name'] == ".ASPXAUTH"):
            COOKIES_ALL[cookie['name']] = cookie
            COOKIES_NV[cookie['name']] = {'name': cookie['name'], 'domain': cookie['domain'], 'value': cookie['value']}

            #COOKIES_NV[cookie['name']] = cookie["value"]

    if len(COOKIES_ALL) != 2:
        raise ValueError('Cookies were returned incorrectly.')

    WD = wd


def convert_to_cookiejar():
    global WD

    CJ = CookieJar()

    for s_cookie in WD.get_cookies():
        expires = None

        if 'expiry' in s_cookie:
            expires = s_cookie['expiry']

        cj_cookie = cookiejar.Cookie(version=0, name=s_cookie['name'], value=s_cookie['value'], port='80', port_specified=False,
                             domain=s_cookie['domain'], domain_specified=True, domain_initial_dot=False,
                             path=s_cookie['path'], path_specified=True, secure=s_cookie['secure'],
                             expires=expires, discard=False, comment=None, comment_url=None, rest=None,
                             rfc2109=False)

        CJ.set_cookie(cj_cookie)


    return CJ


def fetch_webpage(url):
    global COOKIES_NV, TIMEOUT, HEADERS, CJ
    while True:
        try:
            response = requests.get(url, cookies=CJ, headers=HEADERS, timeout=TIMEOUT)
            break
        except requests.RequestException:
            print(str(datetime.now()) + ": GET Timeout occurred. Retrying...")

    #page_text = response.text.encode('utf-8').decode('ascii', 'ignore')

    return response.text

def print_msg(msg):
    print(str(datetime.now()) + ": " + msg)


def httpget_withjs(url):
    global WD
    WD.set_page_load_timeout(TIMEOUT)

    while True:
        try:
            WD.get(url)
            print_msg("SUCCESS: HTTP GET: " + url)
            break
        except selenium.common.exceptions.TimeoutException:
            print_msg("FAIL: HTTP GET: " + url)

    return WD.page_source

    # global COOKIES_ALL, COOKIES_NV
    # #while True:
    #     #try:
    # wd = webdriver.PhantomJS()
    # #wd.get("https://findbolig.nu")
    # #wd.delete_all_cookies()
    # wd.get(url)
    # wd.delete_all_cookies()
    # for cookie in COOKIES_NV:
    #     wd.add_cookie(COOKIES_NV[cookie])
    # wd.get(url)
    # #    break
    #     #except Exception:
    #     #    print(str(datetime.now()) + ": GET Timeout occured. Retrying...")
    #
    # return ""

def httppost(url, building_id):
    global TIMEOUT, HEADERS
    while True:
        try:
            cookies = convert_to_cookiejar()
            r = requests.post(url, json={"buildingId": building_id},
                              cookies=cookies, headers=HEADERS,
                              timeout=TIMEOUT)
            print_msg("SUCCESS: HTTP POST: " + url)
            break
        except requests.RequestException:
            print_msg("FAIL: HTTP POST: " + url)

    return r

#####################################################################
################### PULL REGISTERED BUILDINGS #######################
#####################################################################

def get_registered_buildings(deep_status):
    global BUILDINGS_TOTAL, THREAD_COUNT
    page_text = httpget_withjs("https://findbolig_old.nu/Findbolig-nu/Min-side/ventelisteboliger/opskrivninger?")
    soup = BeautifulSoup(page_text, "html.parser")

    building_list_overview = soup.findAll("tr", class_="rowstyle")
    building_list = []
    #start_position = 1
    for building_list_row in building_list_overview:
        building = Building()
        building.id = str(building_list_row).partition("bid=")[2].partition('"')[0]

        if "boliger" in str(building_list_row):
            building.apartments = str(building_list_row).partition('100px;">')[2].partition('</td>')[0]
        else:
            building.apartments = str(building_list_row.nextSibling).partition('100px;">')[2].partition('</td>')[0]

        building.img_url = str(building_list_row).partition('src="')[2].partition('"')[0]
        building.img_url = '<a href="https://findbolig_old.nu/Ejendomspraesentation.aspx?bid=' + building.id + \
                           '" target="_blank"><img src="https://findbolig_old.nu' + building.img_url + '"></a>'

        building_list.append(building)

        # if start_position == 5:
        #     break
        # start_position+=1

    if not deep_status:
        return building_list

    BUILDINGS_TOTAL = len(building_list)
    print_msg("Found " + str(len(building_list)) + " buildings!")

    #pool = ThreadPool(processes=THREAD_COUNT)
    #building_list = pool.map(get_building_data, building_list)

    #pool.close()
    #pool.join()

    for i in range(0, len(building_list)):
        building_list[i] = get_building_data(building_list[i])

    building_list = sorted(building_list, key=lambda x: x.wlist_loc)

    return building_list


def get_building_data(building):
    global BUILDING_CURR, BUILDINGS_TOTAL

    page_text = httpget_withjs(
        "https://findbolig_old.nu/Ejendomspraesentation/Ejendommen.aspx?bid=" + str(building.id))
    soup = BeautifulSoup(page_text, "html.parser")

    building.name = soup.find(id="ctl00_placeholdercontent_0_LabelBuildingNameTop").string
    building.location = soup.find(id="ctl00_placeholdercontent_0_LabelBuildingAddressTop").string
    building.company = soup.find(id="ctl00_placeholdercontent_2_ImageOwner").get('src', '')
    building.company = '<img src="https://findbolig_old.nu' + building.company + '">'

    response = httppost("https://findbolig_old.nu/Services/WaitlistService.asmx/GetWaitlistRank", building.id)
    building.wlist_loc = int(response.text.partition('nt":')[2].partition('}}')[0])

    print_msg(str(BUILDING_CURR) + "/" + str(BUILDINGS_TOTAL))
    BUILDING_CURR += 1

    building.print_data()

    return building


def print_building_list(building_list, time_elapsed):
    html_table = '<!DOCTYPE html><html lang="en">' \
                 '<link href="../bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">' + \
                 '<div class="container"><table class="table table-hover table-condensed table-bordered text-center">' + \
                 '<thead><tr>' \
                 '<th class="text-center">Building</th>' \
                 '<th class="text-center">Name</th>' \
                 '<th class="text-center">Company</th>' \
                 '<th class="text-center">Location</th>' \
                 '<th class="text-center">Apartments</th>' \
                 '<th class="text-center">Waiting List Location</th></tr></thead><tbody>'

    for building in building_list:
        html_table = html_table + '<tr><td>' + building.img_url + '</td>' + \
                     '<td>' + building.name + '</td>' + \
                     '<td>' + building.company + '</td>' + \
                     '<td>' + building.location + '</td>' + \
                     '<td>' + building.apartments + '</td>' + \
                     '<td>' + str(building.wlist_loc) + '</td></tr>'

    html_table = html_table + '</tbody></table>'
    footer = '<br/>Time elapsed: ' + format(time_elapsed, '.2f') + ' seconds.</div></html>'

    save_report(html_table + footer)


def save_report(html):
    path = os.path.abspath('waiting_list_placements\\' + 'findbolig_old' + datetime.today().strftime("%y%m%d") + '.html')
    url = 'file://' + path

    with open(path, 'w') as f:
        f.write(html)

    webbrowser.open(url, new=2)

#####################################################################
################## PULL UNREGISTERED BUILDINGS ######################
#####################################################################

def pull_unregistered_buildings_list(max_price):
    page_text = fetch_webpage("https://findbolig_old.nu/ventelisteboliger/liste.aspx?&rentmax=" + str(
        max_price) + "&roomsmin=2&showrented=1&showyouth=1&page=1&pagesize=100&sortdir=asc")
    soup = BeautifulSoup(page_text, "html5lib")

    pages = int(math.ceil(int(page_text.partition('ResidencesFoundTop">')[2].partition(' R')[0]) / 100.0))

    building_list = []

    for page in list(range(pages)):
        page_text = self.fetch_webpage("https://findbolig_old.nu/ventelisteboliger/liste.aspx?&rentmax=" + str(
            max_price) + "&roomsmin=2&showrented=1&showyouth=1&page=" + str(page + 1) + "&pagesize=100&sortdir=asc")
        soup = BeautifulSoup(page_text, "html5lib")

        for building_information in soup.findAll("tr", class_="rowstyle"):
            building_link = "https://findbolig_old.nu" + \
                            str(building_information).partition('href="')[2].partition('"><img')[0]
            building_list.append(building_link)

    print("Search is complete.")
    print("Found " + str(len(building_list)) + " buildings.")

    return building_list


def get_unregistered_buildings(self, building_list):
    unregistered_buildings = []
    count = 1
    building_list_total = str(len(building_list) + 1)

    for building_link in building_list:
        print("(" + str(count) + "/" + building_list_total + ")" + "Checking, whether unregistered: " + building_link)
        page_text = self.fetch_webpage(building_link)

        if "intSignedUpResidenceTable([]);" in page_text:
            print("Unregistered!")
            unregistered_buildings.append(building_link)

        count += 1

    return unregistered_buildings

#####################################################################
######################## PULL NEW ROOMS #############################
#####################################################################


def get_new_rooms():
    #soup = BeautifulSoup(page_text, "html.parser")

    building_list = get_registered_buildings(False)
    buildings_with_new_rooms = []
    for building in building_list:
        print("Checking whether building ID " + building.id + " has new rooms...")
        page_text = httpget_withjs(
            "https://findbolig_old.nu/Ejendomspraesentation/Ejendommen.aspx?bid=" + building.id + "&display=mysubs")

        if page_text.count("findbolig_old.nu/gfx/minus.gif") > 0:
            buildings_with_new_rooms.append(building)
            print("Found new rooms!")

    #WD.page_source.count("findbolig_old.nu/gfx/minus.gif")

    # building_list_overview = soup.findAll("tr", class_="rowstyle")
    return ""

