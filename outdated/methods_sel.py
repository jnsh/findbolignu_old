# # -*- coding: utf-8 -*-
# #from http.cookiejar import CookieJar
# #import mechanize
# #from selenium.common.exceptions import TimeoutException
#
# #import copy_selenium_cookies_to_cookiejar
# import os
# import webbrowser
# import requests
# from bs4 import BeautifulSoup
# import math
# from datetime import datetime
# from multiprocessing.dummy import Pool as ThreadPool
# from Building import Building
# #from selenium import webdriver
#
# COOKIES = None
# BUILDING_ANALYZED = 1
# BUILDINGS_TOTAL = 0
# TIMEOUT = 90
#
# class Methods:
#     def __init__(self, username, password):
#         global SESSION
#         self.login(username, password)
#
#     def fetch_webpage(self, url):
#         global COOKIES
#         while True:
#             try:
#                 wd = webdriver.PhantomJS()
#                 wd.set_page_load_timeout(TIMEOUT*1000);
#                 wd.add_cookie({k: COOKIES[k] for k in ('name', 'value', 'domain', 'path', 'expiry') if k in COOKIES})
#                 #wd.add_cookie(COOKIES)
#                 response = wd.get(url)
#                 break
#             except TimeoutException:
#                 print (str(datetime.now()) + ": GET Timeout occured. Retrying...")
#
#         #page_text = response.text.encode('utf-8').decode('ascii', 'ignore')
#         wd.save_screenshot("test.png")
#         return response
#
#     def get_new_rooms(self):
#         page_text = self.fetch_webpage("https://www.findbolig.nu/Ejendomspraesentation/Ejendommen.aspx?bid=1402&display=mysubs")
#         #soup = BeautifulSoup(page_text, "html5lib")
#
#         #building_list_overview = soup.findAll("tr", class_="rowstyle")
#         return ""
#
#     def print_building_list(self, building_list):
#         html_table = '<link href="../bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">' + \
#              '<table class="table table-hover table-condensed table-bordered text-center">' + \
#              '<thead><tr>' \
#              '<th class="text-center">Building</th>' \
#              '<th class="text-center">Name</th>' \
#              '<th class="text-center">Company</th>' \
#              '<th class="text-center">Location</th>' \
#              '<th class="text-center">Apartments</th>' \
#              '<th class="text-center">Waiting List Location</th></tr></thead><tbody>'
#
#         for building in building_list:
#             html_table = html_table + '<tr><td>' + building.img_url + '</td>' + \
#             '<td>' + building.name + '</td>' + \
#             '<td>' + building.company + '</td>' + \
#             '<td>' + building.location + '</td>' + \
#             '<td>' + building.apartments + '</td>' + \
#             '<td>' + str(building.wlist_loc) + '</td></tr>'
#
#         html_table = html_table + '</tbody></table>'
#
#         self.save_report(html_table)
#
#     def login(self, username, password):
#         global COOKIES
#         wd = webdriver.PhantomJS()
#         wd.get("https://www.findbolig.nu/logind.aspx")
#         wd.find_element_by_id("ctl00_placeholdercontent_1_txt_UserName").send_keys(username)
#         wd.find_element_by_id("ctl00_placeholdercontent_1_txt_Password").send_keys(password)
#         wd.find_element_by_id("ctl00_placeholdercontent_1_but_LoginShadow").click()
#
#         COOKIES = wd.get_cookie('.ASPXAUTH')
#
#     def login_old(self, username, password):
#         br = mechanize.Browser()
#         cj = CookieJar()
#         br.set_cookiejar(cj)
#
#         br.addheaders = [('User-agent',
#                           'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36')]
#
#         br.open("https://findbolig.nu/logind.aspx")
#         br.select_form(nr=0)
#         br.set_all_readonly(False)
#
#         br["__EVENTTARGET"] = "ctl00$placeholdercontent_1$but_Login"
#         br["__EVENTARGUMENT"] = ""
#         br["ctl00$placeholdercontent_1$txt_UserName"] = username
#         br["ctl00$placeholdercontent_1$txt_Password"] = password
#         br.submit()
#
#         return cj
#
#     def get_registered_buildings(self):
#         global BUILDINGS_TOTAL
#         page_text = self.fetch_webpage("https://www.findbolig.nu/Findbolig-nu/Min-side/ventelisteboliger/opskrivninger?")
#         soup = BeautifulSoup(page_text, "html.parser")
#
#         building_list_overview = soup.findAll("tr", class_="rowstyle")
#         building_list = []
#
#         for building_list_row in building_list_overview:
#             building = Building()
#             building.id = str(building_list_row).partition("bid=")[2].partition('"')[0]
#
#             building.apartments = str(building_list_row).partition('100px;">')[2].partition('</td>')[0]
#
#             building.img_url = str(building_list_row).partition('src="')[2].partition('"')[0]
#             building.img_url = '<a href="https://findbolig.nu/Ejendomspraesentation.aspx?bid=' + building.id + \
#                                '" target="_blank"><img src="https://findbolig.nu' + building.img_url + '"></a>'
#
#             building_list.append(building)
#
#         BUILDINGS_TOTAL = len(building_list)
#         print ("Found " + str(len(building_list)) + " buildings!")
#
#         pool = ThreadPool(12)
#         building_list = pool.map(self.get_building_data, building_list)
#
#         pool.close()
#         pool.join()
#
#         building_list = sorted(building_list, key=lambda x: x.wlist_loc)
#
#         return building_list
#
#     def get_building_data(self, building):
#         global BUILDING_ANALYZED, BUILDINGS_TOTAL
#
#         page_text = self.fetch_webpage(
#             "https://findbolig.nu/Ejendomspraesentation/Ejendommen.aspx?bid=" + str(building.id))
#         soup = BeautifulSoup(page_text, "html.parser")
#
#         building.name = soup.find(id="ctl00_placeholdercontent_0_LabelBuildingNameTop").string
#
#         building.location = soup.find(id="ctl00_placeholdercontent_0_LabelBuildingAddressTop").string
#
#         building.company = soup.find(id="ctl00_placeholdercontent_2_ImageOwner").get('src', '')
#         building.company = '<img src="https://findbolig.nu' + building.company + '">'
#
#         while True:
#             try:
#                 r = requests.post("https://findbolig.nu/Services/WaitlistService.asmx/GetWaitlistRank",
#                     json={"buildingId": building.id}, cookies=COOKIE, timeout=TIMEOUT)
#                 break
#             except requests.RequestException:
#                 print (str(datetime.now()) + ": POST Timeout occured. Retrying...")
#
#         building.wlist_loc = int(r.text.partition('nt":')[2].partition('}}')[0])
#
#         print (str(datetime.now()) + ": " + str(BUILDING_ANALYZED) + "/" + str(BUILDINGS_TOTAL))
#         BUILDING_ANALYZED += 1
#
#         building.print_data()
#
#         return building
#
#     def save_report(self, html):
#         path = os.path.abspath('waiting_list_placements\\' + 'findbolig' + datetime.today().strftime("%y%m%d") + '.html')
#         url = 'file://' + path
#
#         with open(path, 'w') as f:
#             f.write(html)
#
#         webbrowser.open(url, new=2)
#
#     def pull_unregistered_buildings_list(self, max_price):
#         page_text = self.fetch_webpage("https://findbolig.nu/ventelisteboliger/liste.aspx?&rentmax=" + str(max_price) + "&roomsmin=2&showrented=1&showyouth=1&page=1&pagesize=100&sortdir=asc")
#         soup = BeautifulSoup(page_text, "html5lib")
#
#         pages = int(math.ceil(int(page_text.partition('ResidencesFoundTop">')[2].partition(' R')[0])/100.0))
#
#         building_list = []
#
#         for page in list(range(pages)):
#             page_text = self.fetch_webpage("https://findbolig.nu/ventelisteboliger/liste.aspx?&rentmax=" + str(
#                 max_price) + "&roomsmin=2&showrented=1&showyouth=1&page=" + str(page+1) + "&pagesize=100&sortdir=asc")
#             soup = BeautifulSoup(page_text, "html5lib")
#
#             for building_information in soup.findAll("tr", class_="rowstyle"):
#                 building_link = "https://findbolig.nu" + str(building_information).partition('href="')[2].partition('"><img')[0]
#                 building_list.append(building_link)
#
#         print ("Search is complete.")
#         print ("Found " + str(len(building_list)) + " buildings.")
#
#         return building_list
#
#     def get_unregistered_buildings(self, building_list):
#         unregistered_buildings = []
#         count = 1
#         building_list_total = str(len(building_list)+1)
#
#         for building_link in building_list:
#             print ("(" + str(count) + "/" + building_list_total + ")" + "Checking, whether unregistered: " + building_link)
#             page_text = self.fetch_webpage(building_link)
#
#             if "intSignedUpResidenceTable([]);" in page_text:
#                 print ("Unregistered!")
#                 unregistered_buildings.append(building_link)
#
#             count += 1
#
#         return unregistered_buildings
#
