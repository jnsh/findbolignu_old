from outdated import methods_new
import webbrowser
import time

# VARIABLES
APP_MODE = 0
USERNAME = ""
PASSWORD = ""
MAX_PRICE = 8000

methods_new.print_msg("Starting up with APPMODE = " + str(APP_MODE) + "...")

methods_new.login(USERNAME, PASSWORD)

# Generate sorted list with waiting list placements
if APP_MODE == 0:
    methods_new.httpget_withjs("https://findbolig_old.nu/Findbolig-nu/Min-side/ventelisteboliger/opskrivninger?")

    timer_start = time.time()
    building_list = methods_new.get_registered_buildings(True)

    #methods.print_msg("Found: " + str(len(building_list)) + " buildings!")

    methods_new.print_building_list(building_list, time.time() - timer_start)
# Search and open links for all buildings that are under MAX_PRICE and we are not registered
elif APP_MODE == 1:
    pulled_building_list = methods_new.pull_unregistered_buildings_list(MAX_PRICE)
    unregistered_building_list = methods_new.get_unregistered_buildings(pulled_building_list)

    print ("Found " + str(len(unregistered_building_list)) + " unregistered buildings.")

    for link in unregistered_building_list:
        webbrowser.open(link)
# Open links for all buildings where we have unregistered rooms
elif APP_MODE == 2:
    print ("Cx")

    methods_new.get_new_rooms()
