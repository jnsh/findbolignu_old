class Building:
    id = 0
    img_url = ""
    name = ""
    company = ""
    location = ""
    apartments = ""
    wlist_loc = 0

    def print_data(self):
        print ("id:" + str(self.id) + "_img_url:" + self.img_url + "_name:" + self.name + \
              "_company:" + self.company + "_location:" + self.location + "_apartments:" + self.apartments + \
              "_wlist_loc:" + str(self.wlist_loc))